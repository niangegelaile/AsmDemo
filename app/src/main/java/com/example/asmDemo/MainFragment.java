package com.example.asmDemo;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class MainFragment extends BaseFragment {

    private Button mBut1;
    private Button mBut2;
    private Button mBut3;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        mBut2=view.findViewById(R.id.but2);
        mBut3=view.findViewById(R.id.but3);
        mBut1=view.findViewById(R.id.but1);
        mBut1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission(getActivity(),null);
            }
        });
        mBut2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission1(getActivity(),null);
            }
        });
        mBut3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission2(getActivity(),null);
            }
        });

        return view;
    }

    @AutoPermission(value= Manifest.permission.CAMERA,requestCode = 11)
    public void requestPermission(Activity activity,Integer resultCode){
        if(resultCode==null|| PackageManager.PERMISSION_GRANTED==resultCode){
            Log.d("TAG","已获得权限");
            mBut1.setText("已获得权限");
        }else {
            Toast.makeText(getContext(),"请添加权限！",Toast.LENGTH_SHORT).show();
        }

    }

    @AutoPermission(value=android.Manifest.permission.WRITE_EXTERNAL_STORAGE,requestCode = 12)
    public void requestPermission1(Activity activity,Integer resultCode){
        if(resultCode==null|| PackageManager.PERMISSION_GRANTED==resultCode){
            Log.d("TAG","已获得权限");
            mBut2.setText("已获得权限");
        }else {
            Toast.makeText(getContext(),"请添加权限！",Toast.LENGTH_SHORT).show();
        }
    }

    @AutoPermission(value= Manifest.permission.ACCESS_COARSE_LOCATION,requestCode = 13)
    public void requestPermission2(Activity activity,Integer resultCode){
        if(resultCode==null|| PackageManager.PERMISSION_GRANTED==resultCode){
            Log.d("TAG","已获得权限");
            mBut3.setText("已获得权限");
        }else {
            Toast.makeText(getContext(),"请添加权限！",Toast.LENGTH_SHORT).show();
        }
    }

}
