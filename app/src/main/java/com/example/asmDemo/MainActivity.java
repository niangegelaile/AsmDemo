package com.example.asmDemo;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends BaseActivity {
    private Button mBut1;
    private Button mBut2;
    private Button mBut3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBut2=findViewById(R.id.but2);
        mBut3=findViewById(R.id.but3);
        mBut1=findViewById(R.id.but1);
        mBut1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission(MainActivity.this,null);
            }
        });
        mBut2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission1(MainActivity.this,null);
            }
        });
        mBut3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission2(MainActivity.this,null);
            }
        });

    }

    @AutoPermission(value= Manifest.permission.CAMERA+","+android.Manifest.permission.WRITE_EXTERNAL_STORAGE,requestCode = 11)
    public void requestPermission(Activity activity,Integer resultCode){
        if(resultCode==null||PackageManager.PERMISSION_GRANTED==resultCode){
            Log.d("TAG","已获得权限");
            mBut1.setText("已获得权限");
        }else {
            Toast.makeText(this,"请添加权限！",Toast.LENGTH_SHORT).show();
        }

    }

    @AutoPermission(value=android.Manifest.permission.WRITE_EXTERNAL_STORAGE,requestCode = 12)
    public void requestPermission1(Activity activity,Integer resultCode){
        if(resultCode==null||PackageManager.PERMISSION_GRANTED==resultCode){
            Log.d("TAG","已获得权限");
            mBut2.setText("已获得权限");
        }else {
            Toast.makeText(this,"请添加权限！",Toast.LENGTH_SHORT).show();
        }

    }

    @AutoPermission(value= Manifest.permission.ACCESS_COARSE_LOCATION,requestCode = 13)
    public void requestPermission2(Activity activity,Integer resultCode){
        if(resultCode==null||PackageManager.PERMISSION_GRANTED==resultCode){
            Log.d("TAG","已获得权限");
            mBut3.setText("已获得权限");
        }else {
            Toast.makeText(this,"请添加权限！",Toast.LENGTH_SHORT).show();
        }
    }

}
