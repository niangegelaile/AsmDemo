# AsmDemo

#### 介绍
利用ASM框架插件自动申请android 运行时权限

#### 软件架构
通过ASM框架,给class中的已添加注解@AutoPermission的方法插入字节码（在方法调用前添加权限是否已获取的判断，如果没有则去申请），
然后在class自动补全onRequestPermissionsResult方法，重新执行被注解的方法。


#### 安装教程

1.  在android项目把autoPermission_plugin module 引入
2.  执行autoPermission_plugin 的gradle task uploadArchives
3.  生成./repo 仓库及插件jar(如果你直接复制本项目目录下的repo到你的项目中，可以省略1~3步)
4.  android项目根目录下build.gradle的repositories加入仓库路径 ，maven { url "$rootDir/repo" }
5.  android项目根目录下build.gradle的dependencies 加入 classpath "com.asm.autoPermission:autoPermission_plugin:1.0.0"
6.  app目录的build.gradle 加：apply plugin: 'autoPermission'
7.  app 代码目录里创建AutoPermission 

```
package com.example.asmDemo;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;

@Target(METHOD)
@Retention(RetentionPolicy.CLASS)
public @interface AutoPermission {
    String value();
    int requestCode();
}
```

#### 使用说明

1. 使用例子：
支持在继承androidx.appcompat.app.AppCompatActivity的activity或继承e androidx.fragment.app.Fragment的fragment(Fragment父类的名字一定要包含“Fragment”)中使用：

 申请单个权限：
```
@AutoPermission(value=android.Manifest.permission.WRITE_EXTERNAL_STORAGE,requestCode = 12)
    public void requestPermission1(Activity activity,Integer resultCode){
        if(resultCode==null||PackageManager.PERMISSION_GRANTED==resultCode){
            Log.d("TAG","已获得权限");
            mBut2.setText("已获得权限");
        }else {
            Toast.makeText(this,"请添加权限！",Toast.LENGTH_SHORT).show();
        }

    }
```

申请多个权限：

```
  @AutoPermission(value= Manifest.permission.CAMERA+","+android.Manifest.permission.WRITE_EXTERNAL_STORAGE,requestCode = 11)
    public void requestPermission(Activity activity,Integer resultCode){
        if(resultCode==null||PackageManager.PERMISSION_GRANTED==resultCode){
            Log.d("TAG","已获得权限");
            mBut1.setText("已获得权限");
        }else {
            Toast.makeText(this,"请添加权限！",Toast.LENGTH_SHORT).show();
        }

    }
```

注意：同一个class里如果在多个方法上加注解，注解的requestCode必须是连续的整数，不能 1 ，3  ，7 ，需要1，2，3




#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
