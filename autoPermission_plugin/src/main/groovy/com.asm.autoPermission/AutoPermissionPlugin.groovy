package com.asm.autoPermission

import com.android.build.gradle.AppExtension
import org.gradle.api.Plugin
import org.gradle.api.Project

public class AutoPermissionPlugin implements Plugin<Project> {
    void apply(Project project) {
        System.out.println("==AutoPermissionPlugin gradle plugin==")

        def android = project.extensions.getByType(AppExtension)
        println '----------- registering AutoTrackTransform  -----------'
        AutoPermissionTransform transform = new AutoPermissionTransform()
        android.registerTransform(transform)
    }
}