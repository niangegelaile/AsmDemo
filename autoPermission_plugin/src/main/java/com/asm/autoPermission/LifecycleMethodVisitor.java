package com.asm.autoPermission;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;


import org.objectweb.asm.Label;



import static groovyjarjarasm.asm.Opcodes.*;


public class LifecycleMethodVisitor extends MethodVisitor {
    private String className;
    private String methodName;
    private boolean isTarget;
    private LifecycleClassVisitor classVisitor;
    private String mValue;
    private int requestCode;

    public LifecycleMethodVisitor(MethodVisitor methodVisitor, String className, String methodName) {
        super(Opcodes.ASM5, methodVisitor);
        this.className = className;
        this.methodName = methodName;
    }


    @Override
    public void visitInsn(int opcode) {
        super.visitInsn(opcode);
//        if(isTarget){
//            Label l1 = new Label();
//            mv.visitLabel(l1);
//            mv.visitVarInsn(ILOAD, requestCode);
//            mv.visitInsn(ICONST_1);
//            Label l2 = new Label();
//            mv.visitJumpInsn(IF_ICMPNE, l2);
//            Label l3 = new Label();
//            mv.visitLabel(l3);
//            mv.visitVarInsn(ALOAD, 0);
//            mv.visitVarInsn(ALOAD, 0);
//            mv.visitMethodInsn(INVOKEVIRTUAL, className, "getActivity", "()Landroidx/fragment/app/FragmentActivity;", false);
//            mv.visitMethodInsn(INVOKEVIRTUAL, className, methodName, "(Landroid/app/Activity;)V", false);
//            mv.visitLabel(l2);
//            mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
//        }

    }

    @Override
    public void visitCode() {
        super.visitCode();
        System.out.println("MethodVisitor visitCode------");
        if (isTarget) {
            Label l0 = new Label();
            mv.visitLabel(l0);

            mv.visitVarInsn(ALOAD, 2);
            Label l1 = new Label();
            mv.visitJumpInsn(IFNONNULL, l1);

            String[] permissions=mValue.split(",");
            mv.visitVarInsn(ALOAD, 1);
            mv.visitLdcInsn(permissions[0]);
            mv.visitMethodInsn(INVOKESTATIC, "androidx/core/content/ContextCompat", "checkSelfPermission", "(Landroid/content/Context;Ljava/lang/String;)I", false);
            if (permissions.length > 1) {
                Label l2 = new Label();
                mv.visitJumpInsn(IFNE, l2);
                for (int i = 1; i < permissions.length; i++) {
                    mv.visitVarInsn(ALOAD, 1);
                    mv.visitLdcInsn(permissions[i]);
                    Label l = new Label();
                    mv.visitLabel(l);
                    mv.visitMethodInsn(INVOKESTATIC, "androidx/core/content/ContextCompat", "checkSelfPermission", "(Landroid/content/Context;Ljava/lang/String;)I", false);
                    if (i == permissions.length - 1) {
                        mv.visitJumpInsn(IFEQ, l1);
                        mv.visitLabel(l2);
                        mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
                    } else {
                        mv.visitJumpInsn(IFNE, l2);
                    }
                }
            } else {
                mv.visitJumpInsn(IFEQ, l1);
                Label l2 = new Label();
                mv.visitLabel(l2);
            }


            mv.visitVarInsn(ALOAD, classVisitor.isFragment() ? 0 : 1);
            switch (permissions.length){
                case 1:
                    mv.visitInsn(ICONST_1);
                    break;
                case 2:
                    mv.visitInsn(ICONST_2);
                    break;
                case 3:
                    mv.visitInsn(ICONST_3);
                    break;
                case 4:
                    mv.visitInsn(ICONST_4);
                    break;

            }
            mv.visitTypeInsn(ANEWARRAY, "java/lang/String");
            for(int i=0;i<permissions.length;i++){
                mv.visitInsn(DUP);
                switch (i){
                    case 0:
                        mv.visitInsn(ICONST_0);
                        break;
                    case 1:
                        mv.visitInsn(ICONST_1);
                        break;
                    case 2:
                        mv.visitInsn(ICONST_2);
                        break;
                    case 3:
                        mv.visitInsn(ICONST_3);
                        break;
                    case 4:
                        mv.visitInsn(ICONST_4);
                        break;

                }

                mv.visitLdcInsn(permissions[i]);
                mv.visitInsn(AASTORE);
            }
            mv.visitIntInsn(BIPUSH, requestCode);
            if (classVisitor.isFragment()) {
                mv.visitMethodInsn(INVOKEVIRTUAL, className, "requestPermissions", "([Ljava/lang/String;I)V", false);
            } else {
                mv.visitMethodInsn(INVOKESTATIC, "androidx/core/app/ActivityCompat", "requestPermissions", "(Landroid/app/Activity;[Ljava/lang/String;I)V", false);
            }
            Label l4 = new Label();
            mv.visitLabel(l4);

            mv.visitInsn(RETURN);
            mv.visitLabel(l1);

            mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
//            mv.visitInsn(RETURN);
//            Label l5 = new Label();
//            mv.visitLabel(l5);
        }

    }

    @Override
    public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
        if ("Lcom/example/asmDemo/AutoPermission;".equals(descriptor)) {
            isTarget = true;
            AnnotationValueVisitor annotationValueVisitor= new AnnotationValueVisitor(super.visitAnnotation(descriptor, visible));

            return annotationValueVisitor;
        }else {
            isTarget=false;
        }
        return super.visitAnnotation(descriptor, visible);
    }

    class AnnotationValueVisitor extends AnnotationVisitor {

         AnnotationValueVisitor(AnnotationVisitor annotationVisitor) {
            super(ASM6, annotationVisitor);
        }

        @Override
        public void visit(String name, Object value) {
            System.out.println("注解" + name + value);
            if(name.equals("requestCode")){
                requestCode= (int) value;
            }else {
                mValue= (String) value;
            }
            if(requestCode!=0&&mValue!=null){
                if(classVisitor.isFragment()){
                    System.out.println("classVisitor.hascode="+classVisitor.hashCode()+"methodName="+methodName+",requestCode="+requestCode+",value="+mValue);
                    classVisitor.saveMethodInfo(methodName,requestCode);
                }else {
                    classVisitor.saveMethodInfo(methodName,requestCode);
                }
            }

            super.visit(name, value);
        }
    }


    public void setClassVisitor(LifecycleClassVisitor classVisitor) {
        this.classVisitor = classVisitor;
    }
}