package com.asm.autoPermission;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;


import org.objectweb.asm.Label;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static groovyjarjarasm.asm.Opcodes.*;

public class LifecycleClassVisitor extends ClassVisitor {
    private String className;
    private String superName;
    private boolean isFragment;
    HashMap<Integer,String> mMethodMap;
    public LifecycleClassVisitor(ClassVisitor cv) {
        super(Opcodes.ASM5, cv);
        this.mMethodMap=new HashMap<>();
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(version, access, name, signature, superName, interfaces);
        this.className = name;
        this.superName = superName;
        isFragment=superName.contains("Fragment");
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        System.out.println("ClassVisitor visitMethod name-------" + name + ", superName is " + superName);
        MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);

//        if (superName.equals("androidx/appcompat/app/AppCompatActivity")||superName.equals("androidx/fragment/app/Fragment")) {
            LifecycleMethodVisitor lifecycleMethodVisitor=new LifecycleMethodVisitor(mv, className, name);
            lifecycleMethodVisitor.setClassVisitor(this);
            return lifecycleMethodVisitor;
//        }
//        return mv;
    }


    public void createRequestPermissionsResultMethod(){
        if(mMethodMap.size()!=1){
           return;
        }
        System.out.println("createRequestPermissionsResultMethod methodMap.size="+mMethodMap.size());
        Map.Entry<Integer,String> mapEntry=mMethodMap.entrySet().iterator().next();
        int requestCode=mapEntry.getKey();
        String methodName=mapEntry.getValue();
        System.out.println("createRequestPermissionsResultMethod");
        MethodVisitor mv = this.visitMethod(ACC_PUBLIC, "onRequestPermissionsResult", "(I[Ljava/lang/String;[I)V", null, null);
        mv.visitCode();
        Label l0 = new Label();
        mv.visitLabel(l0);

        mv.visitVarInsn(ALOAD, 0);
        mv.visitVarInsn(ILOAD, 1);
        mv.visitVarInsn(ALOAD, 2);
        mv.visitVarInsn(ALOAD, 3);

        mv.visitMethodInsn(INVOKESPECIAL, superName, "onRequestPermissionsResult", "(I[Ljava/lang/String;[I)V", false);
        Label l1 = new Label();
        mv.visitLabel(l1);

        generateresult(mv);

        mv.visitVarInsn(ILOAD, 1);
        mv.visitIntInsn(BIPUSH, requestCode);
        Label l2 = new Label();
        mv.visitJumpInsn(IF_ICMPNE, l2);
        Label l3 = new Label();
        mv.visitLabel(l3);

        mv.visitVarInsn(ALOAD, 0);
        mv.visitVarInsn(ALOAD, 0);
        if(isFragment){
            mv.visitMethodInsn(INVOKEVIRTUAL, className, "getActivity", "()Landroidx/fragment/app/FragmentActivity;", false);
        }
        mv.visitVarInsn(ILOAD, 4);
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, className, methodName, "(Landroid/app/Activity;Ljava/lang/Integer;)V", false);

        mv.visitLabel(l2);

        mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
        mv.visitInsn(RETURN);
        Label l4 = new Label();
        mv.visitLabel(l4);
        mv.visitLocalVariable("this", "L"+className+";", null, l0, l4, 0);
        mv.visitLocalVariable("requestCode", "I", null, l0, l4, 1);
        mv.visitLocalVariable("permissions", "[Ljava/lang/String;", null, l0, l4, 2);
        mv.visitLocalVariable("grantResults", "[I", null, l0, l4, 3);
        mv.visitMaxs(4, 4);
        mv.visitEnd();


    }


     void saveMethodInfo(String methodName,int requestCode){
        mMethodMap.put(requestCode,methodName);
        System.out.println("saveMethodInfo methodMap.size="+mMethodMap.size());

    }


    private void createRequestPermissionsResultMethodMultiple(){

        if(mMethodMap.size()<2){
            return;
        }
        System.out.println("createRequestPermissionsResultMethodMultiple methodMap.size="+mMethodMap.size());
        List<Integer> mMethodList=new ArrayList<>();
        for(Map.Entry<Integer,String> i:mMethodMap.entrySet()){
            mMethodList.add(i.getKey());
        }
        mMethodList.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer integer, Integer t1) {
                if(integer>t1){
                    return 1;
                }else if(integer<t1){
                    return -1;
                }
                return 0;
            }
        });
        MethodVisitor mv = this.visitMethod(ACC_PUBLIC, "onRequestPermissionsResult", "(I[Ljava/lang/String;[I)V", null, null);
        mv.visitCode();
        Label l0 = new Label();
        mv.visitLabel(l0);

        mv.visitVarInsn(ALOAD, 0);
        mv.visitVarInsn(ILOAD, 1);
        mv.visitVarInsn(ALOAD, 2);
        mv.visitVarInsn(ALOAD, 3);
        mv.visitMethodInsn(INVOKESPECIAL, superName, "onRequestPermissionsResult", "(I[Ljava/lang/String;[I)V", false);

        Label l1 = new Label();
        mv.visitLabel(l1);

        generateresult(mv);



        mv.visitVarInsn(ILOAD, 1);
        Label[] labels=new Label[mMethodMap.size()];

        for(int i=0;i<labels.length;i++){
            Label label = new Label();
            labels[i]=label;

        }

        Label l5 = new Label();

        mv.visitTableSwitchInsn(mMethodList.get(0),mMethodList.get(mMethodList.size()-1), l5, labels);


            for(int i=0;i<labels.length;i++){

                mv.visitLabel(labels[i]);

                mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
                mv.visitVarInsn(ALOAD, 0);
                mv.visitVarInsn(ALOAD, 0);
                if(isFragment){
                    mv.visitMethodInsn(INVOKEVIRTUAL, className, "getActivity", "()Landroidx/fragment/app/FragmentActivity;", false);
                }
                mv.visitVarInsn(ILOAD, 4);
                mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
                mv.visitMethodInsn(INVOKEVIRTUAL, className, mMethodMap.get(mMethodList.get(i)), "(Landroid/app/Activity;Ljava/lang/Integer;)V", false);



                Label l7 = new Label();
                mv.visitLabel(l7);

                mv.visitJumpInsn(GOTO, l5);


            }


        mv.visitLabel(l5);

        mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
        mv.visitInsn(RETURN);
        Label l9 = new Label();
        mv.visitLabel(l9);
        mv.visitLocalVariable("this", "L"+className+";", null, l0, l9, 0);
        mv.visitLocalVariable("requestCode", "I", null, l0, l9, 1);
        mv.visitLocalVariable("permissions", "[Ljava/lang/String;", null, l0, l9, 2);
        mv.visitLocalVariable("grantResults", "[I", null, l0, l9, 3);
        mv.visitMaxs(4, 4);
        mv.visitEnd();
    }


    private void generateresult(MethodVisitor mv ){
        mv.visitInsn(ICONST_0);
        mv.visitVarInsn(ISTORE, 4);
        Label l2 = new Label();
        mv.visitLabel(l2);

        mv.visitInsn(ICONST_0);
        mv.visitVarInsn(ISTORE, 5);
        Label l3 = new Label();
        mv.visitLabel(l3);
        mv.visitFrame(Opcodes.F_APPEND, 2, new Object[]{Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
        mv.visitVarInsn(ILOAD, 5);
        mv.visitVarInsn(ALOAD, 3);
        mv.visitInsn(ARRAYLENGTH);
        Label l4 = new Label();
        mv.visitJumpInsn(IF_ICMPGE, l4);
        Label l5 = new Label();
        mv.visitLabel(l5);

        mv.visitVarInsn(ALOAD, 3);
        mv.visitVarInsn(ILOAD, 5);
        mv.visitInsn(IALOAD);
        Label l6 = new Label();
        mv.visitJumpInsn(IFEQ, l6);
        Label l7 = new Label();
        mv.visitLabel(l7);

        mv.visitVarInsn(ALOAD, 3);
        mv.visitVarInsn(ILOAD, 5);
        mv.visitInsn(IALOAD);
        mv.visitVarInsn(ISTORE, 4);
        Label l8 = new Label();
        mv.visitLabel(l8);

        mv.visitJumpInsn(GOTO, l4);
        mv.visitLabel(l6);

        mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
        mv.visitIincInsn(5, 1);
        mv.visitJumpInsn(GOTO, l3);
        mv.visitLabel(l4);
        mv.visitLineNumber(28, l4);
        mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
    }





    @Override
    public void visitEnd() {
        createRequestPermissionsResultMethodMultiple();
        createRequestPermissionsResultMethod();
        super.visitEnd();
    }

     boolean isFragment() {
        return isFragment;
    }
}